import http from "./httpService";

const getUsers = async query => {
  const res = await http.get(`/users?${http.toQuery(query)}`);
  return res.data;
};

export default {
  getUsers
};
