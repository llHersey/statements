import http from "./httpService";

const getEmployees = projectId => http.get(`/employees/${projectId}`).then(res => res.data);

export default {
  getEmployees
};
