import { toast } from "react-toastify";

const obj = {
  autoClose: 5000,
  closeOnClick: true,
  draggable: true,
  hideProgressBar: false,
  pauseOnHover: true,
  position: "top-right"
};

const showError = text => {
  toast.error(text, { ...obj });
};

const showSuccess = text => {
  toast.success(text, { ...obj });
};

const showInfo = text => {
  toast.info(text, { ...obj });
};

export default {
  showError,
  showInfo,
  showSuccess
};
