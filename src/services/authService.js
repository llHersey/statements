import http from "./httpService";
import jwtDecode from "jwt-decode";

const login = async model => {
  const res = await http.post("/auth/login", model);
  let data = res.data;
  const { token } = data;
  localStorage.setItem("token", token);
  return data;
};

const register = model => http.post("/users/register", model).then(res => res.data);

const logout = () => localStorage.removeItem("token");

const getCurrentUser = () => {
  try {
    const jwt = localStorage.getItem("token");
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
};
const getJWT = () => localStorage.getItem("token");

http.setJWT(getJWT());

export default {
  login,
  logout,
  register,
  getCurrentUser,
  getJWT
};
