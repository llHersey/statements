import http from "./httpService";

const getProjects = filter => http.get(`/projects?${http.toQuery(filter)}`).then(res => res.data);

const getProjectDetails = projectId => http.get(`/projects/${projectId}`).then(res => res.data);

const changePaymentType = (projectId, paymentTypeId) => http.put(`/projects/${projectId}/${paymentTypeId}`).then(res => res.data);

const updateHoursRetainer = (projectId, ammountObj) => http.put(`/retainer/${projectId}`, ammountObj).then(res => res.data);

export default {
  getProjects,
  getProjectDetails,
  changePaymentType,
  updateHoursRetainer
};
