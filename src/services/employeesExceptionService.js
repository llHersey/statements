import http from "./httpService";

const getEmployeesException = (projectId, query) => http.get(`/exceptions/${projectId}?${http.toQuery(query)}`).then(res => res.data);

const updateEmployeeException = empException => http.put("/exceptions", empException).then(res => res.data);

const addEmployeeException = empException => http.post("/exceptions", empException).then(res => res.data);

const deleteEmployeeException = (projectId, employeeId) => http.delete(`/exceptions/${projectId}/${employeeId}`).then(res => res.data);

export default {
  getEmployeesException,
  updateEmployeeException,
  addEmployeeException,
  deleteEmployeeException
};
