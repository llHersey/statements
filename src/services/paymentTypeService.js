import http from "./httpService";

const getPaymentTypes = () => http.get("/paymenttypes").then(res => res.data);

export default {
  getPaymentTypes
};
