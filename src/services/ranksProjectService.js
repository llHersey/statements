import http from "./httpService";

const getRankCostsBtPtoject = (projectId, filter, retainer = false) => {
  if (retainer) return http.get(`/projectsbyrank/retainer/${projectId}?${http.toQuery(filter)}`).then(res => res.data);

  return http.get(`/projectsbyrank/rank/${projectId}?${http.toQuery(filter)}`).then(res => res.data);
};

const updateRankCostByProject = (projectId, rankId, obj, retainer = false) => {
  if (retainer) return http.put(`/projectsbyrank/retainer/${projectId}/${rankId}`, obj).then(res => res.data);
  return http.put(`/projectsbyrank/rank/${projectId}/${rankId}`, obj).then(res => res.data);
};

export default {
  getRankCostsBtPtoject,
  updateRankCostByProject
};
