import http from "./httpService";

const getActivities = filter => http.get(`/activities?${http.toQuery(filter)}`).then(res => res.data);

export default {
  getActivities
};
