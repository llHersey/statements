import Raven from "raven-js";

const config = {
  environment: process.env.REACT_APP_SENTRY_ENV,
  release: process.env.REACT_APP_RELEASE
};

const init = () => {
  if (process.env.REACT_APP_SENTRY_ENV === "Staging" || process.env.REACT_APP_SENTRY_ENV === "Producction")
    Raven.config(process.env.REACT_APP_SENTRY_KEY, config).install();
};

const log = (error, extra) => Raven.captureException(error, extra);

const message = (error, extra) => Raven.captureMessage(error.message, extra);

export default {
  init,
  log,
  message
};
