import axios from "axios";
import httpHelper from "../tools/http";
import RequestProgress from "../tools/requestProgress";
import toast from "./toastService";
import logger from "./logService";

let cancelers = [];

const cancelRequest = () => {
  cancelers.forEach(c => c());
  cancelers = [];
};

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  onUploadProgress: event => {
    RequestProgress.notify({
      percentage: Math.round((event.loaded * 100) / event.total),
      total: event.total
    });

    if (event.loaded === event.total) RequestProgress.complete();
  }
});

const handleError = error => {
  if (axios.isCancel(error)) {
    return Promise.reject({ response: { data: "Cancelado por el usuario" } });
  }

  if (!httpHelper.isExpected(error)) {
    logger.log(error);
    toast.showError("Error inesperado, el administrador ya fué comunicado");
  }
  return Promise.reject(error);
};

instance.interceptors.request.use(
  request => ({
    ...request,
    cancelToken: new axios.CancelToken(c => {
      cancelers.push(c);
    })
  }),
  error => handleError(error)
);

instance.interceptors.response.use(response => response, error => handleError(error));

const toQueryString = obj => {
  const parts = [];
  for (const property in obj) {
    if (obj.hasOwnProperty(property) && (property !== null && property !== undefined)) {
      const value = obj[property];

      if (value !== null && value !== undefined && value !== "") parts.push(`${encodeURIComponent(property)}=${encodeURIComponent(value)}`);
    }
  }
  return parts.join("&");
};

const setJWT = token => {
  instance.defaults.headers.common["Authorization"] = `bearer ${token}`;
};
export default {
  cancelRequest,
  delete: instance.delete,
  get: instance.get,
  isCancel: axios.isCancel,
  post: instance.post,
  put: instance.put,
  toQuery: toQueryString,
  setJWT
};
