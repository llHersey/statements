import React, { Component } from "react";
import { Reactter } from "reactter";
import { Container, Button } from "semantic-ui-react";
import Table from "../../components/table/table";
import TablePagination from "../../components/pagination/pagination";
import UserService from "../../services/userService";

class userTable extends Component {
  state = {
    isLoading: true,
    query: {
      isSortAscending: true,
      page: 1,
      pageSize: 12
    },
    queryResult: {
      items: [],
      totalItems: 0
    }
  };

  columns = [
    { label: "Nombre completo", path: "name", sortKey: "name", content: user => `${user.name} ${user.lastName} ${user.secondLastName}` },
    { label: "Email", path: "email", sortKey: "email" },
    { label: "Roles", path: "roles", sortKey: "roles", fit: true, content: user => this.rolesToString(user) },
    { label: "Editar", path: "edit", sortKey: "edit", content: user => <Button content="Editar" fluid /> }
  ];

  rolesToString = user => {
    if (user.roles.length > 0) return user.roles.map(res => res.description).join(", ");

    return "Sin roles";
  };

  render() {
    const { pageSize, page } = this.state.query;
    const { items, totalItems } = this.state.queryResult;

    return (
      <Container style={styles.container}>
        <div style={styles.tableContainer}>
          <h2>Usuarios del sistema</h2>
          <Table columns={this.columns} data={items} isLoading={this.state.isLoading} />
          <TablePagination onPageChange={this.onPageChange} activePage={page} elementsByPage={pageSize} totalItems={totalItems} />
        </div>
      </Container>
    );
  }

  componentDidMount = async () => await this.populateUsers();

  populateUsers = async () => {
    this.setState({
      isLoading: false,
      queryResult: await UserService.getUsers(this.state.query)
    });
  };

  onPageChange = (e, { activePage }) => this.setState({ isLoading: true }, () => Reactter.updateState("query.page", activePage, this.populateUsers)(this));

  handleChange = (e, { name, value }) => Reactter.updateState("query." + name, value)(this);
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "70%",
    flexDirection: "column",
    padding: "0 15px"
  },
  tableContainer: {
    width: "100%"
  }
};

export default userTable;
