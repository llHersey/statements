import React from "react";
import { Form, Segment } from "semantic-ui-react";
import Input from "./../../components/form/Input";

const activitiesFilter = ({ onResetFilter, onFilterChange, onSearch, code, name, cost }) => (
  <Segment secondary>
    <Form>
      <Form.Group inline widths="equal">
        <Input onChange={onFilterChange} name="code" label="Código" value={code} />
        <Input onChange={onFilterChange} name="name" label="Nombre" value={name} />
        <Form.Field style={{ position: "relative" }}>
          <Form.Input onChange={onFilterChange} name="cost" fluid label="Costo" icon="dollar" iconPosition="left" value={cost} />
        </Form.Field>
      </Form.Group>
      <Form.Group inline>
        <Form.Button compact content={`Reiniciar`} color={"twitter"} onClick={onResetFilter} />
        <Form.Button compact content={`Buscar`} onClick={onSearch} />
      </Form.Group>
    </Form>
  </Segment>
);
export default activitiesFilter;
