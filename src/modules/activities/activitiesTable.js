import React, { Component } from "react";
import { Reactter } from "reactter";
import { Container } from "semantic-ui-react";
import Table from "../../components/table/table";
import TablePagination from "../../components/pagination/pagination";
import ActivitiesFilter from "./activitiesFilter";
import ActivitiesService from "../../services/activitiesService";

class Vehicules extends Component {
  state = {
    isLoading: true,
    query: {
      code: "",
      cost: "",
      name: "",
      isSortAscending: true,
      page: 1,
      pageSize: 12,
      sortkey: "code"
    },
    queryResult: {
      items: [],
      totalItems: 0
    }
  };

  columns = [
    { label: "Código", path: "code", sortKey: "code" },
    { label: "Nombre", path: "name", sortKey: "name" },
    { label: "Es variable", path: "isvariable", sortKey: "isvariable", content: activity => (activity.isVariable ? "Si" : "No") },
    { label: "Costo", path: "cost", sortKey: "cost", content: activity => `$${activity.cost}.00` }
  ];

  render() {
    const { sortBy, isSortAscending, pageSize, page, code, name, cost } = this.state.query;
    const { items, totalItems } = this.state.queryResult;
    const sortColumn = { sortBy, isSortAscending };

    return (
      <Container style={styles.container}>
        <div style={styles.tableContainer}>
          <h2>Actividades</h2>
          <ActivitiesFilter
            onResetFilter={this.resetFilter}
            onSearch={this.populateActivities}
            onFilterChange={this.handleChange}
            cost={cost}
            name={name}
            code={code}
          />
          <Table columns={this.columns} data={items} isLoading={this.state.isLoading} onSort={this.handleSortBy} sortColumn={sortColumn} />
          <TablePagination onPageChange={this.onPageChange} activePage={page} elementsByPage={pageSize} totalItems={totalItems} />
        </div>
      </Container>
    );
  }

  componentDidMount = async () => await this.populateActivities();

  handleSearch = async () => {
    const newState = { ...this.state };
    newState.query.page = 1;
    this.setState(newState, this.populateActivities);
  };

  populateActivities = async () => {
    this.setState({
      isLoading: false,
      queryResult: await ActivitiesService.getActivities(this.state.query)
    });
  };

  handleSortBy = async columnName => {
    const obj = { ...this.state };
    if (this.state.query.sortBy === columnName) {
      obj.query.isSortAscending = !obj.query.isSortAscending;
    } else {
      obj.query.sortBy = columnName;
      obj.query.isSortAscending = true;
    }
    this.setState(obj, this.populateActivities);
  };

  onPageChange = (e, { activePage }) => this.setState({ isLoading: true }, () => Reactter.updateState("query.page", activePage, this.populateActivities)(this));

  handleChange = (e, { name, value }) => Reactter.updateState("query." + name, value)(this);

  resetFilter = () => {
    const obj = { ...this.state };
    obj.query.code = "";
    obj.query.cost = "";
    obj.query.name = "";
    this.setState(obj, this.populateActivities);
  };
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "90%",
    flexDirection: "column",
    padding: "0 15px"
  },
  tableContainer: {
    width: "100%"
  }
};

export default Vehicules;
