import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import { PropTypes } from "prop-types";

class deleteException extends Component {
  static propTypes = {
    projectId: PropTypes.string.isRequired,
    employeeId: PropTypes.string.isRequired
  };

  onClick = () => {
    const { projectId, employeeId } = this.props;
    this.props.onDelete(projectId, employeeId);
  };

  render() {
    return <Button style={{ padding: "4px 18px" }} size="mini" loading={this.props.isLoading} color="red" content="Eliminar" onClick={this.onClick} />;
  }
}

export default deleteException;
