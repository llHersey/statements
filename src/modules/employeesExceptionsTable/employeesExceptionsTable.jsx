import React, { Component } from "react";
import { Reactter } from "reactter";
import { Container, Dropdown, Button } from "semantic-ui-react";
import Table from "../../components/table/table";
import TablePagination from "../../components/pagination/pagination";
import Editable from "../../components/editableLabel/editableLabel";
import EmployeesExceptionService from "../../services/employeesExceptionService";
import DeleteException from "./deleteException";
import { PropTypes } from "prop-types";
import toastService from "../../services/toastService";
import employeesService from "../../services/employeesService";
import http from "../../tools/http";
import { isNullOrUndefined } from "util";

class exceptionsEmployeesTable extends Component {
  static propTypes = { projectId: PropTypes.string.isRequired };

  state = {
    isLoading: true,
    isLoadingAddException: false,
    isLoadingEditException: false,
    isLoadingDeleteException: false,
    query: {
      isSortAscending: true,
      page: 1,
      pageSize: 11,
      sortkey: "code"
    },
    queryResult: {
      items: [],
      totalItems: 0
    },
    employees: [],
    exceptionValue: ""
  };

  columns = [
    { label: "Nombre", path: "employee", content: exception => this.renderName(exception) },
    { label: "Monto", path: "amount", content: exception => this.renderPopup(exception) },
    {
      label: "Eliminar",
      path: "delete",
      fit: true,
      content: ({ projectId, employee }) => (
        <DeleteException
          isLoading={this.state.isLoadingDeleteException}
          projectId={projectId}
          employeeId={employee.gpn}
          onDelete={this.deleteExceptionEmployee}
        />
      )
    }
  ];

  componentDidMount = async () => {
    await this.populateExceptionEmployees();
    await this.populateEmployees();
  };

  renderName = exception => `${exception.employee.name} ${exception.employee.lastname}`;

  renderPopup = exception => (
    <Editable
      loading={this.state.isLoadingEditException}
      title="Digite un nuevo monto"
      onSave={this.onExceptionAmountEdited}
      onSaveParam={exception}
      initialValue={`$${exception.amount}`}
    />
  );

  onExceptionAmountEdited = (newAmount, exception) => {
    if (isNullOrUndefined(newAmount) || newAmount === "") {
      toastService.showError("Inserte un monto válido");
      return;
    }

    this.setState({ isLoadingEditException: true });

    const updateException = {
      projectId: exception.projectId,
      employeeId: exception.employee.gpn,
      amount: newAmount
    };

    EmployeesExceptionService.updateEmployeeException(updateException).then(
      data => {
        toastService.showSuccess("Excepción actualizada a $" + data.amount);
        this.setState({ isLoadingEditException: false });
        this.populateExceptionEmployees();
      },
      error => {
        if (http.isExpected(error) || http.isNotFound(error)) toastService.showError(error.response);
      }
    );
  };

  deleteExceptionEmployee = (projectId, employeeId) => {
    EmployeesExceptionService.deleteEmployeeException(projectId, employeeId).then(async () => {
      const query = { ...this.state.query };
      query.page = 1;

      this.setState({ query });

      await this.populateExceptionEmployees();
      await this.populateEmployees();
    });
  };

  populateExceptionEmployees = async () => {
    this.setState({ isLoading: true });

    const data = await EmployeesExceptionService.getEmployeesException(this.props.projectId, this.state.query);

    this.setState({
      isLoading: false,
      queryResult: data
    });
  };

  populateEmployees = async () => {
    this.setState({ isLoadingAddException: true });
    const data = await employeesService.getEmployees(this.props.projectId);

    this.setState({
      isLoadingAddException: false,
      employees: data
    });
  };

  onPageChange = (e, { activePage }) =>
    this.setState({ isLoading: true }, () => Reactter.updateState("query.page", activePage, this.populateExceptionEmployees)(this));

  onExceptionSearchChange = (e, { value }) => Reactter.updateState("exceptionValue", value)(this);

  handleAddException = () => {
    if (this.state.exceptionValue === "" || isNullOrUndefined(this.state.exceptionValue)) {
      toastService.showError("Seleccione un empleado primero");
      return;
    }
    this.setState({ isLoadingAddException: true });
    const exception = {
      projectId: this.props.projectId,
      employeeId: this.state.exceptionValue,
      amount: 0
    };

    EmployeesExceptionService.addEmployeeException(exception).then(async () => {
      this.setState({ exceptionValue: "" });
      await this.populateEmployees();
      await this.populateExceptionEmployees();
    });
  };

  render() {
    const { pageSize, page } = this.state.query;
    const { items, totalItems } = this.state.queryResult;

    const options = this.state.employees.map(emp => ({
      text: `${emp.gpn} - ${emp.name} ${emp.lastname}`,
      value: emp.gpn
    }));

    return (
      <Container style={styles.container}>
        <div style={styles.tableContainer}>
          <div style={{ display: "flex" }}>
            <div style={{ display: "flex", flexGrow: 3 }}>
              <Dropdown
                fluid
                options={options}
                search
                selection
                placeholder="Buscar empleado..."
                onChange={this.onExceptionSearchChange}
                noResultsMessage="Empleado no encontrado"
                value={this.state.exceptionValue}
              />
            </div>
            <div style={{ display: "flex", flexGrow: 1 }}>
              <Button fluid className="buttonEY" content="Agregar" onClick={this.handleAddException} loading={this.state.isLoadingAddException} />
            </div>
          </div>
          <Table columns={this.columns} data={items} keyColumn="employee.gpn" isLoading={this.state.isLoading} />
          <div>
            <div>
              {items.length > 0 && <TablePagination onPageChange={this.onPageChange} activePage={page} elementsByPage={pageSize} totalItems={totalItems} />}
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "90%",
    flexDirection: "column",
    padding: "0 0px"
  },
  tableContainer: {
    width: "100%"
  }
};

export default exceptionsEmployeesTable;
