import React from "react";
import HomeBackground from "../../assets/images/HomeBackground.jpg";

const styles = {
  default: {
    height: "100vh",
    background: `url(${HomeBackground}) no-repeat center center fixed`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed",
    overflow: "hidden"
  }
};

const Home = ({ children, style }) => {
  return <div style={{ ...styles.default, ...style }}>{children}</div>;
};

export default Home;
