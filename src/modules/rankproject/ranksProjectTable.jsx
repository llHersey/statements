import React, { Component } from "react";
import { Reactter } from "reactter";
import { Container } from "semantic-ui-react";
import Table from "../../components/table/table";
import TablePagination from "../../components/pagination/pagination";
import Editable from "../../components/editableLabel/editableLabel";
import RanksProjectService from "../../services/ranksProjectService";
import { PropTypes } from "prop-types";
import toastService from "../../services/toastService";
import http from "../../tools/http";
import { isNullOrUndefined } from "util";

class ranksProjectTable extends Component {
  static propTypes = { projectId: PropTypes.string.isRequired };

  state = {
    isLoading: true,
    isLoadingRank: false,
    query: {
      isSortAscending: true,
      page: 1,
      pageSize: 12,
      sortkey: "code"
    },
    queryResult: {
      items: [],
      totalItems: 0
    }
  };

  columns = [{ label: "Rank", path: "rank.description" }, { label: "Costo", path: "amount", content: rankProject => this.renderPopup(rankProject) }];

  renderPopup = rankProject => (
    <Editable
      loading={this.state.isLoadingRank}
      title="Digite un nuevo monto"
      onSave={this.onRankPriceEdited}
      onSaveParam={rankProject}
      initialValue={`$${rankProject.amount}`}
    />
  );

  onRankPriceEdited = (amount, rankProject) => {
    if (isNullOrUndefined(amount) || amount === "") {
      toastService.showError("Inserte un monto válido");
      return;
    }

    this.setState({ isLoadingRank: true });

    const updateRank = {
      amount
    };

    const projectId = this.props.projectId;
    const rankId = rankProject.rank.id;

    RanksProjectService.updateRankCostByProject(projectId, rankId, updateRank, this.props.retainer).then(
      data => {
        toastService.showSuccess("Rank actualizado a $" + data.amount);
        this.setState({ isLoadingRank: false });
        this.populateProjectRanks();
      },
      error => {
        if (http.isExpected(error) || http.isNotFound(error)) toastService.showError(error.response);
      }
    );
  };

  render() {
    const { pageSize, page } = this.state.query;
    const { items, totalItems } = this.state.queryResult;

    return (
      <Container style={styles.container}>
        <div style={styles.tableContainer}>
          <Table columns={this.columns} data={items} keyColumn="rank.id" isLoading={this.state.isLoading} />
          <TablePagination onPageChange={this.onPageChange} activePage={page} elementsByPage={pageSize} totalItems={totalItems} />
        </div>
      </Container>
    );
  }

  componentDidMount = async () => await this.populateProjectRanks();

  populateProjectRanks = async () => {
    this.setState({ isLoading: true });

    let data = [];

    data = await RanksProjectService.getRankCostsBtPtoject(this.props.projectId, this.state.query, this.props.retainer);

    this.setState({
      isLoading: false,
      queryResult: data
    });
  };

  onPageChange = (e, { activePage }) =>
    this.setState({ isLoading: true }, () => Reactter.updateState("query.page", activePage, this.populateProjectRanks)(this));
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "90%",
    flexDirection: "column",
    padding: "0 15px"
  },
  tableContainer: {
    width: "100%"
  }
};

export default ranksProjectTable;
