import { Component } from "react";
import authService from "../../../services/authService";

class logout extends Component {
  componentDidMount() {
    authService.logout();
    window.location = "/";
  }
  render() {
    return null;
  }
}

export default logout;
