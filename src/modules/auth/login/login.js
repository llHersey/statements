import React, { Component } from "react";
import { Segment, Form, Button } from "semantic-ui-react";
import Home from "./../../home/home";
import LogoEYLaw from "../../../assets/images/LogoEYLaw.png";
import Input from "./../../../components/form/Input";
import { Reactter } from "reactter";
import toastService from "../../../services/toastService";
import authService from "../../../services/authService";
import http from "../../../tools/http";

class login extends Component {
  state = {
    data: {
      email: "",
      password: ""
    },
    loading: false,
    errors: {}
  };

  schema = {
    data: {
      email: [Reactter.required, Reactter.email],
      password: [Reactter.required]
    }
  };

  handleChange = (e, { name, value }) => Reactter.updateStateErrors(name, value, this.schema)(this);

  onSubmit = async () => {
    const result = Reactter.validate(this.state, this.schema);

    if (!result.valid) {
      toastService.showError(result.error[0].details[0]);
      return;
    }
    this.setState({ loading: true });

    authService
      .login(this.state.data)
      .then(() => {
        window.location = "/";
      })
      .catch(err => {
        if (http.isUnautorized(err)) {
          toastService.showError("Credenciales invalidas");
          return;
        }
      })
      .then(() => this.setState({ loading: false }));
  };

  render() {
    const { data, errors, loading } = this.state;
    return (
      <Home style={styles.container}>
        <Segment style={styles.card}>
          <div style={styles.logoContainer}>
            <img src={LogoEYLaw} alt="EYLogo" style={styles.logo} />
          </div>
          <Form style={styles.formContainer} inverted>
            <Input name="data.email" label="Email" onChange={this.handleChange} error={errors.email} value={data.email} />
            <Input name="data.password" type="password" label="Contraseña" onChange={this.handleChange} error={errors.password} value={data.password} />
          </Form>
          <div style={styles.buttonContainer}>
            <Button loading={loading} content="Log in" style={styles.button} onClick={this.onSubmit} />
          </div>
        </Segment>
      </Home>
    );
  }
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    width: "100%",
    flexDirection: "column",
    padding: "0 15px"
  },
  card: {
    display: "flex",
    height: "700px",
    width: "40%",
    flexDirection: "column",
    marginLeft: "50px",
    marginRight: "50px",
    padding: "40px",
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  logoContainer: {
    display: "flex",
    flexGrow: "3",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    width: "70%"
  },
  formContainer: {
    display: "flex",
    flexGrow: "1",
    flexDirection: "column",
    justifyContent: "center"
  },
  buttonContainer: {
    display: "flex",
    flexGrow: "3",
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    width: "100%",
    height: "40px",
    backgroundColor: "#ffe400"
  }
};

export default login;
