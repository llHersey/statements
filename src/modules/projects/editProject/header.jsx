import React from "react";
import { Loader, Dimmer } from "semantic-ui-react";

const header = ({ name, loading, financial, manager, partner }) => {
  return (
    <Dimmer.Dimmable blurring dimmed={loading}>
      <div className="info">
        <Loader active={loading} />
        {name && (
          <div style={{ display: "flex", flexDirection: "column" }}>
            <strong className="title">Proyecto</strong>
            <strong className="title" style={{ color: "#ffe400", marginTop: 0 }}>
              {name}
            </strong>
          </div>
        )}
        <div className="otherInfo">
          {financial && <div>Financial: {`${financial.name} ${financial.lastname}`}</div>}
          {manager && <div style={{ margin: "10px 0" }}>Manager: {`${manager.name} ${manager.lastname}`}</div>}
          {partner && <div>Partner: {`${partner.name} ${partner.lastname}`}</div>}
        </div>
      </div>
    </Dimmer.Dimmable>
  );
};

export default header;
