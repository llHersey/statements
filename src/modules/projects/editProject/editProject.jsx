import React, { Component } from "react";
import Header from "./header";
import { Container } from "semantic-ui-react";
import { Rank, Retainer, FlatRate, FlatFee } from "./paymentTypes";
import PaymentTypeButtons from "./paymentTypeButtons";
import projectsService from "../../../services/projectsService";
import toastService from "../../../services/toastService";
import paymentTypeService from "../../../services/paymentTypeService";
import "./editProject.css";

class editProject extends Component {
  state = {
    isLoading: true,
    project: {},
    activePayment: {},
    paymentTypes: [],
    changing: false,
    loadingPayment: {
      loading: false,
      paymentId: 0
    }
  };

  componentDidMount() {
    paymentTypeService.getPaymentTypes().then(res => this.setState({ paymentTypes: res }));

    projectsService
      .getProjectDetails(this.props.match.params.id)
      .then(res => this.setState({ project: res, activePayment: res.paymentType, isLoading: false }))
      .catch(error => toastService.showError(error.response));
  }

  changePaymentType = payment => {
    this.setState({ loadingPayment: { loading: true, paymentId: payment.id } });

    const activePayment = this.state.paymentTypes.find(pt => pt.id === payment.id);
    const projectId = this.props.match.params.id;

    projectsService
      .changePaymentType(projectId, activePayment.id)
      .then(() => this.setState({ activePayment, changing: true, loadingPayment: { loading: false } }));
  };

  stopChanging = () => this.setState({ changing: false });

  render() {
    const { id } = this.props.match.params;
    const { manager, partner, financial, name, retainerHours } = this.state.project;
    const { activePayment, changing, paymentTypes, loadingPayment, isLoading } = this.state;

    return (
      <React.Fragment>
        <Header manager={manager} financial={financial} partner={partner} name={name} loading={isLoading} />
        <Container className="datacontainer">
          <PaymentTypeButtons
            paymentTypes={paymentTypes}
            activePayment={activePayment}
            loadingPayment={loadingPayment}
            changePayment={this.changePaymentType}
          />
          <Rank projectId={id} stopChange={this.stopChanging} isChanging={changing} activePayment={activePayment} />
          <Retainer projectId={id} stopChange={this.stopChanging} hours={retainerHours} isChanging={changing} activePayment={activePayment} />
          <FlatRate projectId={id} stopChange={this.stopChanging} isChanging={changing} activePayment={activePayment} />
          <FlatFee projectId={id} stopChange={this.stopChanging} isChanging={changing} activePayment={activePayment} />
        </Container>
      </React.Fragment>
    );
  }
}

export default editProject;
