import React from "react";
import { Transition, Input } from "semantic-ui-react";

const flatRate = ({ stopChange, activePayment, isChanging }) => {
  return (
    <Transition onComplete={stopChange} animation="scale" duration={500} visible={activePayment.description === "Flat rate" && !isChanging}>
      <div style={{ fontFamily: "EYinterstate-light", paddingTop: "20px" }}>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <div>
            <Input icon="dollar" iconPosition="left" placeholder="Monto del flat rate" action="Guardar" size="large" />
          </div>
        </div>
      </div>
    </Transition>
  );
};

export default flatRate;
