import React from "react";
import { Transition, Form, Button } from "semantic-ui-react";
import Input from "./../../../../components/form/Input";

class flatFee extends React.Component {
  state = {
    data: {
      includeExpenses: false
    }
  };

  toggleExpenses = () => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        includeExpenses: !prevState.data.includeExpenses
      }
    }));
  };

  render() {
    const { stopChange, activePayment, isChanging } = this.props;
    return (
      <Transition onComplete={stopChange} animation="scale" duration={500} visible={activePayment.description === "Flat fee" && !isChanging}>
        <div style={{ fontFamily: "EYinterstate-light" }}>
          <div style={{ display: "flex", flexDirection: "inherit", justifyContent: "center" }}>
            <Form style={{ width: "500px" }}>
              <Form.Group inline widths="equal">
                <Form.Checkbox width="10" label="¿Incluye gastos?" checked={this.state.data.includeExpenses} onChange={this.toggleExpenses} />
                <Input iconPosition="left" icon="dollar" size="large" name="ammount" />
              </Form.Group>
              <Button className="buttonEY" fluid content="Guardar" />
            </Form>
          </div>
        </div>
      </Transition>
    );
  }
}

export default flatFee;
