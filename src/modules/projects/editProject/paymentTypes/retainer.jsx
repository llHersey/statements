import React, { Component } from "react";
import RanksProjectTable from "./../../../rankproject/ranksProjectTable";
import EmployeesExceptionTable from "./../../../employeesExceptionsTable/employeesExceptionsTable";
import { Transition, Button, Form } from "semantic-ui-react";
import Input from "./../../../../components/form/Input";
import { Reactter } from "reactter";
import projectsService from "../../../../services/projectsService";
import toastService from "../../../../services/toastService";
import http from "../../../../tools/http";

class retainer extends Component {
  state = {
    data: {
      hours: ""
    },
    errors: {},
    loading: false
  };

  schema = {
    data: {
      hours: [Reactter.required, Reactter.number]
    }
  };

  componentWillReceiveProps(nextProps) {
    const state = { ...this.state };
    state.data.hours = nextProps.hours;
    this.setState(state);
  }

  componentWillUpdate(nextProps) {
    return nextProps.hours !== this.props.hours;
  }

  handleChange = (e, { name, value }) => Reactter.updateStateErrors("data." + name, value, this.schema)(this);

  onHoursSave = () => {
    const hours = this.state.data.hours;

    projectsService
      .updateHoursRetainer(this.props.projectId, { hours })
      .then(res => {
        toastService.showSuccess("Horas retainer actualizadas a " + res.hours);
      })
      .catch(err => {
        if (http.isExpected(err) || http.isNotFound(err)) toastService.showError(err.response);
      });
  };

  render() {
    const { projectId, stopChange, activePayment, isChanging } = this.props;
    const { hours } = this.state.data;
    const { loading, errors } = this.state;

    return (
      <Transition onComplete={stopChange} animation="scale" duration={500} visible={activePayment.description === "Retainer" && !isChanging}>
        <div style={{ fontFamily: "EYinterstate-light", paddingTop: "20px" }}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <div style={{ display: "flex", flexDirection: "inherit", alignItems: "center" }}>
              <Form style={{ width: "500px" }}>
                <Form.Group inline widths="equal">
                  <Input iconPosition="left" icon="time" label="Horas" name="hours" onChange={this.handleChange} error={errors.hours} value={hours} />
                </Form.Group>
                <Button className="buttonEY" fluid content="Guardar limite de horas" loading={loading} />
              </Form>
            </div>
            <div style={{ display: "flex", marginTop: "60px" }}>
              <div style={{ display: "flex", flexGrow: 2, flexDirection: "column", alignItems: "center" }}>
                <h2>Horas de rank</h2>
                <RanksProjectTable projectId={projectId} />
              </div>
              <div style={{ display: "flex", flexGrow: 2, flexDirection: "column", alignItems: "center" }}>
                <h2>Horas retainer</h2>
                <RanksProjectTable projectId={projectId} retainer />
              </div>
              <div style={{ display: "flex", flexGrow: 2, flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
                <h2>Excepciónes de Empleados</h2>
                <EmployeesExceptionTable projectId={projectId} />
              </div>
            </div>
          </div>
        </div>
      </Transition>
    );
  }
}

export default retainer;
