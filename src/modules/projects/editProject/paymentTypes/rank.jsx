import React from "react";
import RanksProjectTable from "./../../../rankproject/ranksProjectTable";
import EmployeesExceptionTable from "./../../../employeesExceptionsTable/employeesExceptionsTable";
import { Transition } from "semantic-ui-react";

const rank = ({ stopChange, isChanging, activePayment, projectId }) => {
  return (
    <Transition onComplete={stopChange} animation="scale" duration={500} visible={activePayment.description === "Rank" && !isChanging}>
      <div style={{ fontFamily: "EYinterstate-light", paddingTop: "20px" }}>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <div style={{ display: "flex", flexGrow: 2, flexDirection: "column", alignItems: "center" }}>
            <h2>Ranks</h2>
            <RanksProjectTable projectId={projectId} />
          </div>
          <div style={{ display: "flex", flexGrow: 2, flexDirection: "column", alignItems: "center" }}>
            <h2>Excepciones</h2>
            <EmployeesExceptionTable projectId={projectId} />
          </div>
        </div>
      </div>
    </Transition>
  );
};

export default rank;
