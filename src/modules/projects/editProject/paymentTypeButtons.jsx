import React from "react";
import { Button } from "semantic-ui-react";

const paymentTypeButtons = ({ paymentTypes, activePayment, loadingPayment, changePayment }) => {
  return (
    <React.Fragment>
      <div className="paymentTypeText">Tipo de pago para proyecto</div>
      <div className="paymentType">
        <div>
          {paymentTypes.map(payment => (
            <Button
              toggle
              loading={loadingPayment.loading && loadingPayment.paymentId === payment.id}
              key={payment.id}
              active={payment.id === activePayment.id}
              onClick={() => changePayment(payment)}
              content={payment.description}
              style={{ margin: "0 20px" }}
            />
          ))}
        </div>
      </div>
    </React.Fragment>
  );
};

export default paymentTypeButtons;
