import React from "react";
import { Form, Segment } from "semantic-ui-react";
import Input from "./../../components/form/Input";

const activitiesFilter = ({ onResetFilter, onFilterChange, onSearch, projectid, name, client, manager, partner, financial }) => (
  <Segment secondary>
    <Form>
      <Form.Group inline widths="equal">
        <Input onChange={onFilterChange} name="id" label="Código" value={projectid} />
        <Input onChange={onFilterChange} name="name" label="Nombre" value={name} />
        <Input onChange={onFilterChange} name="client" label="Cliente" value={client} />
      </Form.Group>
      <Form.Group inline widths="equal">
        <Input onChange={onFilterChange} name="manager" label="Manager" value={manager} />
        <Input onChange={onFilterChange} name="partner" label="Partner" value={partner} />
        <Input onChange={onFilterChange} name="financial" label="Financial" value={financial} />
      </Form.Group>
      <Form.Group inline>
        <Form.Button compact content={`Reiniciar`} color={"twitter"} onClick={onResetFilter} />
        <Form.Button compact content={`Buscar`} onClick={onSearch} />
      </Form.Group>
    </Form>
  </Segment>
);
export default activitiesFilter;
