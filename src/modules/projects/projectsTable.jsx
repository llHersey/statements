import React, { Component } from "react";
import { Reactter } from "reactter";
import { Container, Button } from "semantic-ui-react";
import Table from "../../components/table/table";
import TablePagination from "../../components/pagination/pagination";
import ProjectsFilter from "./projectsFilter";
import ProjectsService from "../../services/projectsService";
import { Link } from "react-router-dom";

class projectsTable extends Component {
  state = {
    isLoading: true,
    query: {
      id: "",
      name: "",
      client: "",
      manager: "",
      financial: "",
      partner: "",
      isSortAscending: true,
      page: 1,
      pageSize: 9,
      sortkey: "id"
    },
    queryResult: {
      items: [],
      totalItems: 0
    }
  };

  columns = [
    { label: "Código", path: "id", sortKey: "id" },
    { label: "Nombre", path: "name", sortKey: "name" },
    { label: "Cliente", path: "client.name", sortKey: "client" },
    { label: "Manager", path: "manager.name", sortKey: "manager" },
    { label: "Partner", path: "partner.name", sortKey: "partner" },
    { label: "Financial", path: "financial.name", sortKey: "financial" },
    { label: "Estado", path: "projectStatus.description", sortKey: "status" },
    { label: "Tipo de pago", path: "paymentType.description", sortKey: "paymentType" },
    { label: "Editar", path: "edit", content: project => <Button as={Link} color="green" to={`/projects/${project.id}`} fluid content="Editar" /> }
  ];

  render() {
    const { sortBy, isSortAscending, pageSize, page, id, name, manager, partner, financial, client } = this.state.query;
    const { items, totalItems } = this.state.queryResult;
    const sortColumn = { sortBy, isSortAscending };

    return (
      <Container style={styles.container}>
        <div style={styles.tableContainer}>
          <h2>Proyectos</h2>

          <ProjectsFilter
            onResetFilter={this.resetFilter}
            onSearch={this.populateProjects}
            onFilterChange={this.handleChange}
            name={name}
            projectid={id}
            client={client}
            manager={manager}
            partner={partner}
            financial={financial}
          />
          <Table columns={this.columns} data={items} isLoading={this.state.isLoading} onSort={this.handleSortBy} sortColumn={sortColumn} />
          <TablePagination onPageChange={this.onPageChange} activePage={page} elementsByPage={pageSize} totalItems={totalItems} />
        </div>
      </Container>
    );
  }

  componentDidMount = async () => await this.populateProjects();

  handleSearch = async () => {
    const state = { ...this.state };
    state.query.page = 1;
    this.setState(state, this.populateProjects);
  };

  populateProjects = async () => {
    this.setState({ isLoading: true });
    const projects = await ProjectsService.getProjects(this.state.query);
    this.setState({
      isLoading: false,
      queryResult: projects
    });
  };

  handleSortBy = async columnName => {
    const obj = { ...this.state };

    if (this.state.query.sortBy === columnName) {
      obj.query.isSortAscending = !obj.query.isSortAscending;
    } else {
      obj.query.sortBy = columnName;
      obj.query.isSortAscending = true;
    }
    this.setState(obj, this.populateProjects);
  };

  onPageChange = (e, { activePage }) => this.setState({ isLoading: true }, () => Reactter.updateState("query.page", activePage, this.populateProjects)(this));

  handleChange = (e, { name, value }) => Reactter.updateState("query." + name, value)(this);

  resetFilter = () => {
    const obj = { ...this.state };

    obj.query.id = "";
    obj.query.name = "";
    obj.query.client = "";
    obj.query.manager = "";
    obj.query.partner = "";
    obj.query.financial = "";
    obj.query.page = 1;

    this.setState(obj, this.populateProjects);
  };
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "90%",
    flexDirection: "column",
    padding: "0 15px"
  },
  tableContainer: {
    width: "100%"
  }
};

export default projectsTable;
