import React from "react";
import { Transition } from "semantic-ui-react";

const Error = ({ message, time, animation, display }) => {
  return (
    <Transition.Group duration={time} animation={animation}>
      {message && display && <span style={{ color: "red", margin: 0 }}>{message}</span>}
    </Transition.Group>
  );
};

Error.defaultProps = {
  animation: "drop",
  time: 500
};

export default Error;
