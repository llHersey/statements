import React, { Component } from "react";
import logService from "../../services/logService";

class MainErrorBoundary extends Component {
  state = {};

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    logService.log(error, errorInfo);
  }

  render() {
    return this.state.error ? <div>Error</div> : this.props.children;
  }
}

export default MainErrorBoundary;
