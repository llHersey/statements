import React, { Component } from "react";
import { Table } from "semantic-ui-react";

class TableHeaderCell extends Component {
  handleHeaderClick = () => {
    const { sortKey, onClick, label } = this.props;
    if (sortKey && onClick && label) {
      onClick(sortKey);
    }
  };

  render() {
    const { sortKey, sortColumn, label } = this.props;
    const isSameColumn = sortColumn && sortColumn.sortBy === sortKey;
    const sortDirection = sortColumn && sortColumn.isSortAscending ? "ascending" : "descending";
    const sorted = isSameColumn ? sortDirection : undefined;
    return <Table.HeaderCell sorted={sorted} content={label} onClick={this.handleHeaderClick} />;
  }
}

export default TableHeaderCell;
