import React from "react";
import { Table } from "semantic-ui-react";
import TableHeaderCell from "./tableHeaderCell";

const TableHeader = props => {
  return (
    <Table.Header>
      <Table.Row>
        {props.columns.map(c => (
          <TableHeaderCell key={c.path} onClick={props.onSort} sortColumn={props.sortColumn} path={c.path} sortKey={c.sortKey} label={c.label} />
        ))}
      </Table.Row>
    </Table.Header>
  );
};

export default TableHeader;
