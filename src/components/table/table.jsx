import React from "react";
import { Table as NormalTable, Dimmer, Loader, Segment } from "semantic-ui-react";
import TableBody from "./tableBody";
import TableHeader from "./tableHeader";

const InnerTable = ({ columns, sortable, sortColumn, data, onSort, style, keyColumn }) => {
  return (
    <NormalTable sortable={sortable} celled={true} style={style} striped stackable>
      <TableHeader sortColumn={sortColumn} columns={columns} onSort={onSort} />
      <TableBody columns={columns} data={data} keyColumn={keyColumn} />
    </NormalTable>
  );
};

const Table = props => {
  const { columns, sortable, sortColumn, data, onSort, style, isLoading, keyColumn } = props;
  return (
    <Dimmer.Dimmable>
      <Dimmer inverted active={isLoading}>
        <Loader size="small" />
      </Dimmer>
      {data.length > 0 ? (
        <InnerTable columns={columns} keyColumn={keyColumn} sortable={sortable} sortColumn={sortColumn} data={data} onSort={onSort} style={style} />
      ) : (
        <Segment
          secondary
          clearing
          style={{
            height: 100,
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <h3>No hay elementos</h3>
        </Segment>
      )}
    </Dimmer.Dimmable>
  );
};

Table.defaultProps = {
  sortable: true,
  keyColumn: "id",
  data: []
};

export default Table;
