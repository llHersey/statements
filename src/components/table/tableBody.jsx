import _ from "lodash";
import React, { Component } from "react";
import { Table } from "semantic-ui-react";

class TableBody extends Component {
  render() {
    const { data, columns, keyColumn } = this.props;
    return (
      <Table.Body>
        {data.map(item => (
          <Table.Row key={_.get(item, keyColumn)}>
            {columns.map(column => (
              <Table.Cell collapsing={column.fit} key={column.path} content={this.renderCell(item, column)} />
            ))}
          </Table.Row>
        ))}
      </Table.Body>
    );
  }

  renderCell = (item, column) => {
    if (column.content) return column.content(item);

    return _.get(item, column.path);
  };
}

export default TableBody;
