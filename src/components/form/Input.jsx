import React from "react";
import Error from "../error/error";
import { Form, Input as SInput } from "semantic-ui-react";
import PropTypes from "prop-types";
import { isNullOrUndefined } from "util";
import "./Input.css";

const Input = ({ error, name, label, type, displayError, ...props }) => {
  return (
    <Form.Field>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <label className="inputLabel">{label}</label>
        <SInput fluid error={!isNullOrUndefined(error)} name={name} type={type} {...props} />
        <Error display={displayError} message={error} />
      </div>
    </Form.Field>
  );
};

Input.propTypes = {
  error: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  displayError: PropTypes.bool,
  onChange: PropTypes.func
};

Input.defaultProps = {
  type: "text",
  displayError: true
};

export default Input;
