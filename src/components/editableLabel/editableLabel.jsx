import React, { Component } from "react";
import { Popup, Header, Grid, Button } from "semantic-ui-react";
import Input from "./../form/Input";
import { Reactter } from "reactter";

class editableLabel extends Component {
  state = {
    value: "",
    errors: {}
  };

  schema = {
    value: [Reactter.number]
  };

  onSave = () => {
    this.props.onSave(this.state.value, this.props.onSaveParam);
  };

  onChange = (e, { name, value }) => Reactter.updateStateErrors("value", value, this.schema)(this);

  render() {
    const { title, loading } = this.props;
    const { value, errors } = this.state;
    return (
      <Popup trigger={<span style={{ cursor: "pointer", display: "block" }}>{this.props.initialValue}</span>} flowing hoverable>
        <Grid centered>
          <Grid.Column textAlign="center">
            <Header as="h4">{title}</Header>
            <Input error={errors.value} onChange={this.onChange} value={value} />
            <Button loading={loading} style={{ marginTop: "5px" }} onClick={this.onSave} fluid color="green">
              Guardar
            </Button>
          </Grid.Column>
        </Grid>
      </Popup>
    );
  }
}

export default editableLabel;
