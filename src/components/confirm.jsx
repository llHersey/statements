import React from "react";
import { Confirm } from "semantic-ui-react";

const confirm = ({ content, handleCancel, handleConfirm }) => {
  return <Confirm header={content} open={this.state.open} onCancel={handleCancel} onConfirm={handleConfirm} size="tiny" />;
};

export default confirm;
