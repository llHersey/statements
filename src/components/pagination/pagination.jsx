import React from "react";
import { Pagination } from "semantic-ui-react";

const TablePagination = ({ activePage, onPageChange, totalItems, elementsByPage }) => {
  const total = Math.ceil(totalItems / elementsByPage);
  return <Pagination activePage={activePage} onPageChange={onPageChange} totalPages={total} style={{ margin: "10px 0" }} />;
};

export default TablePagination;
