import React from "react";
import { Route, Redirect } from "react-router";
import authService from "../../services/authService";

const anonymousRoute = ({ component: Component, render, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (authService.getCurrentUser()) return <Redirect to="/" />;
        return Component ? <Component /> : render(props);
      }}
    />
  );
};

export default anonymousRoute;
