import React from "react";
import { Menu, Header } from "semantic-ui-react";
import NavBarItem from "./navbarItem";
import LogoEY from "../../assets/images/LogoEY.png";

const NavBarDesktop = ({ items, login, logout, auth, user }) => (
  <Menu style={{ width: "220px" }} inverted fixed="left" vertical>
    <Menu.Item>
      <Header as="h3" inverted image={LogoEY} />
    </Menu.Item>

    {user && <Menu.Item content={`Bienvenido ${user.name}`} />}

    {user && auth.map(item => <NavBarItem key={item.content} {...item} />)}

    {items.map(item => (
      <NavBarItem key={item.content} {...item} />
    ))}

    {user && logout.map(item => <NavBarItem key={item.content} {...item} />)}

    {!user && login.map(item => <NavBarItem key={item.content} {...item} />)}
  </Menu>
);

export default NavBarDesktop;
