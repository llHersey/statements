import React, { Component } from "react";
import { Responsive } from "semantic-ui-react";
import NavBarDesktop from "./navbarDesktop";
import NavBarMobile from "./navbarMobile";

class Layout extends Component {
  state = {
    visible: false
  };

  render() {
    const { items, login, logout, auth, children, user } = this.props;
    const { visible } = this.state;

    return (
      <React.Fragment>
        <Responsive {...Responsive.onlyMobile}>
          <NavBarMobile
            items={items}
            login={login}
            logout={logout}
            auth={auth}
            user={user}
            onPusherClick={this.handlePusher}
            onToggle={this.handleToggle}
            visible={visible}
          >
            <div style={{ width: "100%", height: "100%", overflow: "auto" }}>{children}</div>
          </NavBarMobile>
        </Responsive>
        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
          <NavBarDesktop items={items} login={login} logout={logout} auth={auth} user={user} />
          <div style={{ width: "100%", height: "100vh", paddingLeft: "220px" }}>{children}</div>
        </Responsive>
      </React.Fragment>
    );
  }

  handlePusher = () => {
    const { visible } = this.state;
    if (visible) this.setState({ visible: false });
  };

  handleToggle = () => this.setState({ visible: !this.state.visible });
}

export default Layout;
