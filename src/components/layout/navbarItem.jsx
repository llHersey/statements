import React from "react";
import { NavLink } from "react-router-dom";
import { Menu } from "semantic-ui-react";

const NavBarItem = props => {
  return <Menu.Item as={NavLink} exact={true} to={props.to} content={props.content} />;
};

export default NavBarItem;
