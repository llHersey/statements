import React from "react";
import { Container } from "semantic-ui-react";

const NavBarChildren = ({ children }) => <Container style={{ marginTop: "5em", height: "100%" }}>{children}</Container>;

export default NavBarChildren;
