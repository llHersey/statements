import React from "react";
import { Icon, Menu, Sidebar, Header } from "semantic-ui-react";
import NavBarItem from "./navbarItem";
import LogoEY from "../../assets/images/LogoEY.png";

const NavBarMobile = ({ onPusherClick, onToggle, visible, children, items, login, logout, auth, user }) => (
  <Sidebar.Pushable>
    <Sidebar inverted vertical as={Menu} animation="overlay" icon="labeled" visible={visible} width="thin">
      <Menu.Item>
        <Header as="h3" inverted image={LogoEY} />
      </Menu.Item>
      {user && logout.map(item => <NavBarItem key={item.content} {...item} />)}
      {!user && login.map(item => <NavBarItem key={item.content} {...item} />)}
    </Sidebar>
    <Sidebar.Pusher dimmed={visible} onClick={onPusherClick} style={{ height: "100%" }}>
      <Menu fixed="top" inverted={true}>
        <Menu.Item onClick={onToggle}>
          <Icon name="sidebar" />
        </Menu.Item>

        <Menu.Menu position="right">
          {user && <Menu.Item content={user.content} />}

          {user && auth.map(item => <NavBarItem key={item.content} {...item} />)}

          {items.map(item => (
            <NavBarItem key={item.content} {...item} />
          ))}
        </Menu.Menu>
      </Menu>
      {children}
    </Sidebar.Pusher>
  </Sidebar.Pushable>
);

export default NavBarMobile;
