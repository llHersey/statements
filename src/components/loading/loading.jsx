import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

const Loading = ({ isLoading, inverted }) => {
  return (
    <Dimmer inverted={inverted} active={isLoading}>
      <Loader inverted={inverted} />
    </Dimmer>
  );
};

export default Loading;
