import React, { Component } from "react";
import Loading from "react-loading-bar";

class loadingBar extends Component {
  state = {};

  componentWillMount() {
    this.setState({
      loading: true
    });
  }

  componentWillUnmount() {
    this.setState({
      loading: false
    });
  }
  render() {
    return <Loading show={this.state.loading} showSpinner={false} color="#ffe400" />;
  }
}

export default loadingBar;
