export default class Subject {
  handlers = [];

  startTraking(handler) {
    this.handlers.push(handler);
  }

  endTracking() {
    this.handlers = [];
  }

  notify(data) {
    this.handlers.slice(0).forEach(h => h(data));
  }

  complete() {
    this.handlers = [];
  }

  expose() {
    return this;
  }
}
