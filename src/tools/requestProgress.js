import Subject from "./subject";

class RequestProgress {
  uploadProgress;

  createUploadProgress() {
    this.uploadProgress = new Subject();
    return this.uploadProgress;
  }

  notify(progress) {
    if (this.uploadProgress) {
      this.uploadProgress.notify(progress);
    }
  }

  complete() {
    if (this.uploadProgress) this.uploadProgress.complete();
  }
}

export default new RequestProgress();
