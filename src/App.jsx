import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router";
import { ToastContainer } from "react-toastify";
import ErrorBoundary from "./components/error/errorBoundary";
import Layout from "./components/layout/layout";
import authService from "./services/authService";
import ProtectedRoute from "./components/protectedRoute/protectedRoute";
import AnonymousRoute from "./components/anonymousRoute/anonymousRoute";
import Home from "./modules/home/home";
import Login from "./modules/auth/login/login";
import Logout from "./modules/auth/logout/logout";
import ActivitiesTable from "./modules/activities/activitiesTable";
import ProjectsTable from "./modules/projects/projectsTable";
import EditProject from "./modules/projects/editProject/editProject";
import UsersTable from "./modules/users/userTable";
import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import "semantic-ui-css/semantic.min.css";

const items = [
  {
    content: "Inicio",
    to: "/"
  }
];

const login = [
  {
    content: "Login",
    to: "/login"
  }
];

const logout = [
  {
    content: "Logout",
    to: "/logout"
  }
];

const auth = [
  {
    content: "Actividades",
    to: "/activities"
  },
  {
    content: "Usuarios",
    to: "/users"
  },
  {
    content: "Proyectos",
    to: "/projects"
  }
];

class App extends Component {
  state = {};
  componentDidMount() {
    const user = authService.getCurrentUser();
    this.setState({ user });
  }

  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <ErrorBoundary>
          <Layout items={items} auth={auth} logout={logout} login={login} user={this.state.user}>
            <Switch>
              <Route exact={true} path="/" component={Home} />
              <ProtectedRoute exact path="/activities" component={ActivitiesTable} />
              <ProtectedRoute exact path="/projects" component={ProjectsTable} />
              <ProtectedRoute exact path="/projects/:id" component={EditProject} />
              <ProtectedRoute exact path="/Users" component={UsersTable} />
              <ProtectedRoute exact path="/logout" component={Logout} />
              <AnonymousRoute exact path="/login" component={Login} />
              <Redirect to="/" />
            </Switch>
          </Layout>
        </ErrorBoundary>
      </React.Fragment>
    );
  }
}

export default App;
